package com.example.demo.controler;

import java.util.List;

import javax.management.RuntimeErrorException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.models.Employee;
import com.example.demo.models.Student;
import com.example.demo.services.StudentService;

@RestController
@RequestMapping("/student")
public class StudentController {
	
	@Autowired
	private StudentService studentService;
	
	@GetMapping("/getAllStudents")
	public List<Student> getAll(){
		return this.studentService.getAllStudents();
	}
	
	@PostMapping("/saveStudent")
	public Student save(@RequestBody Student student) {
		return this.studentService.saveStudent(student);
	}
	
	
	@GetMapping("/getStudentById/{id}")
	public Student getStudnetById(@PathVariable Integer id) {
		return this.studentService.getStudentById(id);

	}
	
	@DeleteMapping("/deleteStudentById/{id}")
	public Integer delete(@PathVariable Integer id) {
		studentService.deleteById(id);
		return 1;
	}
	
	@PutMapping("updateStudent")
	public Student update(@RequestBody Student student) {
		return this.studentService.saveStudent(student);
	}
	

}
