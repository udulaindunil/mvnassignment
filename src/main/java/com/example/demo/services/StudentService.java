package com.example.demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.StudentDao;
import com.example.demo.models.Student;

@Service
public class StudentService {

	@Autowired
	public StudentDao studentDao;
	
	public List<Student> getAllStudents(){
		return this.studentDao.findAll();
	}
	
	public Student saveStudent(Student student) {
		return this.studentDao.saveAndFlush(student);
	}

	 public void deleteById(Integer id) {
		this.studentDao.deleteById(id);
	}

	public Student getStudentById(Integer id) {
		return this.studentDao.getOne(id);
	}
	
}
